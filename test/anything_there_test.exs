defmodule AnythingThereTest do
  use ExUnit.Case
  use Maru.Test, for: AnythingThere.API
  doctest AnythingThere

  @doc """
  Tests if the correct api version is returned
  """
  test "/api/version" do
    resp = %Plug.Conn{} = conn(:get, "/api/version") |> make_response
    assert resp.status === 200
    {:ok, vsn} = :application.get_key(:anything_there, :vsn)
    assert resp.resp_body() === "{\"version\":\"#{vsn}\"}"
  end

  test "/index.html" do
    resp = %Plug.Conn{} = conn(:get, "/index.html") |> make_response
    assert resp.status === 200
  end
end
