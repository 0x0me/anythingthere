require Logger

defmodule AnythingThere do
    @moduledoc """
    # ATT? - AnyThing There?
    is an Internet of Things monitoring and management application.
    """

    @doc """
    Current ATT? version
    """
    @version Mix.Project.config[:version]
    def version(), do: @version
end

defmodule AnythingThere.UpdServer do
  @moduledoc """
  # ATT? UDP Server
  Implements the UDP Server for AnyThing There?.
  """

  use GenServer

  def start_link(opts \\ [])  do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init (:ok) do
    udp_options = [
      :binary,
      active: 10,
      reuseaddr: true
    ]
    port = 10001
    {:ok, _socket} = :gen_udp.open(port, udp_options)
  end

  def terminate(reason, state) do
    Logger.debug("-- reason: "<>Atom.to_string(reason))
    Logger.debug("-- state: "<>Atom.to_string(state))
    Logger.info("Terminated")
 end

  def handle_info({:udp, socket, ip, port, "+++quit"}, _) do
    IO.inspect [ip, port]
    Logger.info "QUIT REVC"
    {:stop, :normal , :gen_udp.close(socket)}
  end

  def handle_info({:udp, socket, ip, port, data}, state) do
    :inet.setopts(socket, [active: 1])
    IO.inspect [ip, port, data]
    IO.inspect state
    {:noreply, state}
  end
end

defmodule AnythingThere.Router.Homepage do
  use Maru.Router

  get "/api/version" do
    Logger.debug("API: getting version returns #{AnythingThere.version()}")
    json conn, %{version: AnythingThere.version()}
  end
end

defmodule AnythingThere.Static do
  use Plug.Builder

  # provide anything below local path 'priv/static' on root
  plug Plug.Static, at: "/", from: :anything_there
  plug :not_found

  def not_found(conn, err) do
    Logger.error("AnythingThere.Static: ERROR")
    err |> IO.inspect
    send_resp(conn, 404, "Not Found :-(")
  end
end

defmodule AnythingThere.API do
  use Maru.Router

  mount AnythingThere.Router.Homepage
  mount AnythingThere.Static

  rescue_from :all, as: err do
    Logger.error("AnythingThere.API: ERROR")
    err |> IO.inspect
    send_resp(conn, 500, "Internal Server Error")
  end

end
